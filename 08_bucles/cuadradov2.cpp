
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define NEGRO_T    "\x1b[30m"
#define s 15000
// Función punto de entrada
int main(){

      int l = 15;

    for(int f=0; f<l; f++){
        for(int c=0; c<l; c++)
            if(f == 0 || c == 0 || f == l - 1 || c == l - 1 || f == c || (f+c)==l-1){
            printf(NEGRO_T"  🤖");
            usleep(s);
            }
            else{
                printf("    ");
            }
        printf("\n");
    }
    return EXIT_SUCCESS;
}
