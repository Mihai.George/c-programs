
#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    int pascal[100];
    int n;
    int x, i, j;

    printf("¿Cuantos niveles desea desplegar?: ");
    scanf("%d", &n);
    x=0;

    for(i=1; i<=n; i++){
        for(j=x; j>=0; j--)
        {
            if(j==x || j==0)
            {
                pascal[j]=1;
            }
            else
            {
                pascal[j]=pascal[j]+pascal[j-1];
            }

    }
    x++;
    printf("\n\n");

      for(j=1; j<=n-1; j++)
        printf("   ");

      for(j=0; j<x; j++)
      {
        printf("  %d  ", pascal[j]);
      }
    }

        printf("\n");
    return EXIT_SUCCESS;
}
