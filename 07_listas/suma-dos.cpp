
#include <stdio.h>
#include <stdlib.h>
#define N 2000000
// Función punto de entrada
int main(){


    int emento[N];

    emento[1] = emento[0] = 1;
    /*calculos*/
    for(int i = 2; i<N; i++)
        emento[i] = emento[i-1] + emento[i-2];
    /*salida de datos*/
    for(int i = 0; i<N; i++)
    printf("%i\n", emento[i]);

    printf("\n");

    for(int i=1; i<N; i++)
        printf("%.169lf\n", (double) emento[i] / emento[i-1]);
    printf("\n");

    return EXIT_SUCCESS;
}
