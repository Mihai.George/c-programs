
#include <stdio.h>
#include <stdlib.h>

#define ROJO 4
#define AMAR 2
#define AZUL 1
// Función punto de entrada
int main(){

    char respuesta;
    int color = 0;

    printf(" Ves el rojo? (s/n)");
    scanf(" %c" , &respuesta);
    if (respuesta == 's')
        color |= ROJO;

    printf(" Ves el amarillo? (s/n)");
    scanf(" %c" , &respuesta);
    if (respuesta == 's')
        color |= AMAR;

    printf(" Ves el azul? (s/n)");
    scanf(" %c" , &respuesta);
    if (respuesta == 's')
        color |= AZUL;

    switch(color){

          case 0:
            printf("Color Negro.\n");
            break;
          case 1:
            printf("Color Azul.\n");
            break;
          case 2:
            printf("Color Amarillo.\n");
            break;
          case 3:
            printf("Color Verde.\n");
            break;
          case 4:
            printf("Color Rojo.\n");
            break;
          case 5:
            printf("Color Morado.\n");
            break;

          case 6:
            printf("Color Naranja.\n");
            break;
          case 7:
            printf("Color Blanco.\n");
            break;

    }





    return EXIT_SUCCESS;
}
