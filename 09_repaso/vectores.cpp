#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Función punto de entrada
int main(){

    double X,
           Y,
           X1,
           Y1,
           r;

    /*Entrada de datos*/
    printf("¿Cuanto mide X?\n");
    scanf(" %lf", &X);
    printf("¿Cuanto mide Y?\n");
    scanf(" %lf", &Y);
    printf("¿Cuanto mide X1?\n");
    scanf(" %lf", &X1);
    printf("¿Cuanto mide Y1?\n");
    scanf(" %lf", &Y1);

    /*Calculo de datos*/
    r=sqrt((pow(X,2)+pow(Y,2))*(pow(X1,2)+pow(Y1,2)));

    /*Salida de datos*/
    printf("El vector vale: %.2lf\n", r);

    return EXIT_SUCCESS;
}
