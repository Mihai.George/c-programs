
#include <stdio.h>
#include <stdlib.h>
#define N 10
// Función punto de entrada
int main(){

    int numero[N];

    /*Entrada de datos */
    /*Calculos*/

    for(int i=0; i<N; i++)
        numero[i] = (i + 1) * (i + 1);

    /*Salida de Datos*/
    for(int i=0; i<N; i++)
        printf("%i ", numero[i]);
    printf("\n");
    return EXIT_SUCCESS;
}
