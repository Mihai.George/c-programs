
#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    printf("%c\n", 97);
    printf("%c\n", 0x61);
    printf("%c\n", 'a');

    printf("%i\n", 97);
    /*
    4 => '4'
    4 + '0'
    4 + 0x30
    scanf("%i")
    '4'
    '4'-'0' =>
    '7'
    4*10 =>
    '7'-'0' => 7
    40 + 7 =47
*/

    return EXIT_SUCCESS;
}
