
#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    int dia, mes, annio;
    printf("Introduce tu fecha de nacimiento dd/mm/aaaa: ");
    scanf("%i/%i/%i\n", &dia, &mes, &annio);
    printf("Naciste el %i/%i/%i\n", dia, mes, annio);

    return EXIT_SUCCESS;
}
