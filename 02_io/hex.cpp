#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){
    int numero;

    printf(" Dime el numero que quieras cambiar: ");
    scanf(" %i", &numero);
    printf(" Este es el numero hexadecimal 0x%x\n", numero);

    return EXIT_SUCCESS;
}
