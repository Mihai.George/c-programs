#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 40
#define BTU   100000 /* Basic Time Unit */

// Función punto de entrada
int main(){
    int duracion[VECES] = {1, 1, 1, 5, 5, 2, 2, 2, 4, 4, 3, 3, 3, 3, 4, 4, 2, 2, 2, 5, 5, 1, 1, 1};

    /* printf tiene escritura bufferizada*/

    // bucle for ; Debe valer para repetir cosas
    for(int i=0; i<VECES; i++) {
        usleep(duracion[i] * BTU);
        fputc('\a', stderr);
    }

    return EXIT_SUCCESS;
}
